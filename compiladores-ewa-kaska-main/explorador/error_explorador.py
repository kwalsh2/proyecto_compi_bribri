class ErrorExplorador:
	def __init__(self, palabra, columna, fila):
		self.palabra = palabra
		self.columna = columna
		self.fila = fila

	def __str__(self):
		resultado = f'{self.palabra} Fila: {self.fila} Columna: {self.columna}'
		return resultado
