from explorador.explorador import Elemento


class Analizador:

	# 	#Las funciones en el lenguaje bribi comienza con un kewe(begin)
	# def verificarContenidoFuncion(self):
	def __init__(self, listaElementos):

		self.elementosLexicos = listaElementos
		self.cantidadElementos = len(listaElementos)

		self.posicionElementoActual = 0

		self.elementoActual = listaElementos[0]

	def analizar(self):
		"""
		Método principal que inicia el análisis
		"""
		self.analizarPrograma()

	def analizarPrograma(self):
		"""
		Programa ::= (Asignación | Función | Comentario)* Principal
		"""

		while (True):
			if self.elementoActual.tipo == Elemento.identificador:
				#estamos en una invocación de función
				self.verificarParametros()
			elif (self.elementoActual.texto == 'del'):
				self.siguienteElementoLexico()
				self.analizarFuncion()
			else:
				break
		if (self.elementoActual.texto == 'tsa'):
			self.analizarPrincipal()

	def verificarValoresParametros(self):
		linea_actual = self.elementoActual.fila
		while not self.verificarUltimoParentesis() and self.elementoActual.fila == linea_actual:
			if (not self.verificarValor()):
				print(f"Error de sintaxis: parametro inválido fila {self.elementoActual.col} columna {self.elementoActual.col}")
				quit()
			self.siguienteElementoLexico()

		if(not self.verificarUltimoParentesis()):
			print(f"Error de sintaxis: falta un ')' para la invocación de función fila {self.elementoActual.col} columna {self.elementoActual.col}")
			quit()

	def verificarParametros(self):
		self.siguienteElementoLexico()
		if (self.verificarPrimerParentesis()):
			self.siguienteElementoLexico()
			self.verificarValoresParametros()
		else:
			print(f"Error de sintaxis: falta un ')' para la invocación de función fila {self.elementoActual.col} columna {self.elementoActual.col}")
			quit()


	def verificarIdentificador(self):
		return self.elementoActual.tipo == Elemento.identificador

	def analizarFuncion(self):
		# Se verifica que la función este creada correctamente
		if (self.elementoActual.tipo == Elemento.identificador):
			self.siguienteElementoLexico()
			self.verificarArgumentos()
			self.verificarCuerpoFuncion()
		else:
			# Si no es una palabra(nombre de la función) se muestra un error
			print(f"Error de sintaxis: Se esperaba un indentificador, fila: {self.elementoActual.fila} columna: {self.elementoActual.col} \n valor encontrado: {self.elementoActual.texto}")
			quit()

	def verificarPrimerParentesis(self):
		return self.elementoActual.texto == "("

	def verificarUltimoParentesis(self):
		return self.elementoActual.texto == ')'

	def verificarArgumentos(self):
		#Verificamos los argumentos que hay en la función que se está analizando
		if(self.verificarPrimerParentesis()):
			self.siguienteElementoLexico()
			#recorremos los argumentos porque pueden existir más de 1
			self.verificarIdentificadorArgumentos()
		else:
			#En nuestro lenguaje los arguementos empiezan con un '(', si no es así se muestra un error
			print(f"Error de sintaxis: Se esperaba un '(' en la fila {self.elementoActual.fila} columna {self.elementoActual.col}")
			quit()
		if(not self.verificarUltimoParentesis()):
			print(f"Error de sintaxis: Se esperaba un ')' en la fila {self.elementoActual.fila} columna {self.elementoActual.col}")
			quit()
		self.siguienteElementoLexico()

	#Verifica el nombre de cada variable de la función
	def verificarIdentificadorArgumentos(self):
		# Esta variable nos indica que el elemento léxico que se está analizando está an la misma fila donde se declaró la función
		fila_actual = self.elementoActual.fila
		while (not self.verificarUltimoParentesis() and self.elementoActual.fila == fila_actual):
			if self.elementoActual.tipo != Elemento.identificador:
				print(f"Error de sintaxis: Se esperaba un indentificador, fila: {self.elementoActual.fila} columna: {self.elementoActual.col} \n valor encontrado: {self.elementoActual.texto}")
				quit()
			self.siguienteElementoLexico()

	def verificarComparador(self):
		return self.elementoActual.tipo == Elemento.comparador


	def verificarValor(self):
		return self.elementoActual.tipo == Elemento.identificador or self.elementoActual.tipo == Elemento.entero or self.elementoActual.tipo == Elemento.flotante or self.elementoActual.tipo == Elemento.booleano

	def verificarPalabraCiclo(self):
		return self.elementoActual.tipo == 'dalekua'

	def verificarFinalFuncion(self):
		self.elementoActual.texto == 'bata'


	def verificarCiclo(self):
		if (self.verificarPalabraCiclo()):
			self.siguienteElementoLexico()
		else:
			#Retornamos false porque puede que la instrucción a verificar no sea un loop, sino más bien
			#otra instrucción válida como por ejemplo una asignación
			return False

		if (self.verificarIdentificador()):
			self.siguienteElementoLexico()
		else:
			print(f"Error de sintaxis: Se esperaba un indentificador en la fila:{self.elementoActual.fila} columna {self.elementoActual.col}")
			quit()

		if (self.verificarComparador()):
			self.siguienteElementoLexico()
		else:
			print(f"Error de sintaxis: Se esperaba un comparador en la fila:{self.elementoActual.fila} columna {self.elementoActual.col}")
			quit()

		if (self.verificarIdentificador() or self.verificarValor()):
			self.siguienteElementoLexico()
		else:
			print(f"Error de sintaxis: Se esperaba una variable o valor en la fila:{self.elementoActual.fila} columna {self.elementoActual.col}")
			quit()

	def verificarCuerpoFuncion(self):
		if (self.elementoActual.texto == 'kewe'):
			self.siguienteElementoLexico()
			self.verificarIdentificador()
			self.siguienteElementoLexico()

			#En esta parte se revisa las distintas instrucciones que se pueden realizar en una función
			#como por ejemplo ciclos, asignaciones etc
			if(self.verificarSimboloAsignacion()):
				self.verificarAsignacion()
			elif(self.elementoActual.tipo == 'dalekua'):
				#estamos en un for loop
				self.verificarCiclo()
		else:
			print(f"Error de sintaxis: Las funciones deben contener un inicio(kewe) fila:{self.elementoActual.fila} columna {self.elementoActual.col}")
			quit()

		self.verificarRetornoFuncion()
		if(not self.verificarFinalFuncion()):
			print(f"Error de sintaxis: Las funciones deben contener un final(bata) fila:{self.elementoActual.fila} columna {self.elementoActual.col}")
			quit()
		self.siguienteElementoLexico()

	def verificarSimboloAsignacion(self):
		if (self.elementoActual.texto == 'tse'):
			return True
		else:
			print(f"Error de sintaxis: Se esperaba un elemento de asignación(tse) en la fila:{self.elementoActual.fila} columna {self.elementoActual.col}, \n se encontró {self.elementoActual.texto}")
			quit()

	def verificarPalabraRetorno(self):
		return self.elementoActual.texto == 'dokmale'

	def verificarRetornoFuncion(self):
		if (self.verificarPalabraRetorno()):
			self.siguienteElementoLexico()
			if (self.verificarIdentificador()):
				self.siguienteElementoLexico()
			else:
				print(f"Error de sintaxis: Se esperaba un indentificador o valor, fila: {self.elementoActual.fila} columna: {self.elementoActual.col} \n valor encontrado: {self.elementoActual.texto}")
				quit()
		else:
			print(f"Error de sintaxis: Se esperaba un operador, fila: {self.elementoActual.fila} columna: {self.elementoActual.col} \n valor encontrado: {self.elementoActual.texto}")
			quit()


	def verificarFinalFuncion(self):
		return self.elementoActual.texto == 'bata'

	def verificarAsignacion(self):
		self.siguienteElementoLexico()
		if(self.verificarValor()):
			self.siguienteElementoLexico()
			# Esta variable nos indica que el elemento léxico que se está analizando está an la misma fila donde se declaró la función
			fila_actual = self.elementoActual.fila
			#Variable para saber si lo que debemos analizar es un valor o identificador
			# Ejemplo: a = 1 + 2, nótese que los valores siempre van de por medio
			# se analiza el 1, luego el '+', y luego el 2
			#la variable es_identificador_o_valor sería verdadera cuando se analiza el 1 y el 2
			es_identificador_o_valor = False
			while(self.elementoActual.fila == fila_actual and not self.verificarFinalFuncion()):
				if (not es_identificador_o_valor or not self.verificarValor()):
					self.siguienteElementoLexico()
				es_identificador_o_valor = not es_identificador_o_valor

				if(not self.verificarFinalFuncion()): self.siguienteElementoLexico()
		else:
			print(f"Error de sintaxis: Se esperaba un elemento de asignación(tse) en la fila:{self.elementoActual.fila} columna {self.elementoActual.col}, \n se encontró {self.elementoActual.texto}")
			quit()

	def siguienteElementoLexico(self):
		self.posicionElementoActual += 1

		if (self.posicionElementoActual < len(self.elementosLexicos)):
			self.elementoActual = self.elementosLexicos[self.posicionElementoActual]